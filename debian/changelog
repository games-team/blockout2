blockout2 (2.5+dfsg1-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.
  * Fix field name cases in debian/control (VCS-Browser => Vcs-Browser, VCS-Git
    => Vcs-Git).
  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 22 Oct 2022 16:56:00 -0000

blockout2 (2.5+dfsg1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - refresh patches and drop obsolete one
    - adjust d/Makefile to new paths
  * Update Standards-Version to 4.4.0:
    - change priority from extra to optional
    - declare that d/rules does not require root
  * Honour CPPFLAGS and enable all hardening options.
  * Update debhelper compat level to 12.
  * Point Vcs-* fields to salsa.
  * Fix spelling errors found by lintian.
  * Document orig.tar.xz in README.source.
  * Convert d/copyright to copyright format 1.0.
  * Install README.txt.

 -- Reiner Herrmann <reiner@reiner-h.de>  Fri, 23 Aug 2019 22:59:57 +0200

blockout2 (2.4+dfsg1-9) unstable; urgency=medium

  Apply 882536 patch, thanks to Helmut Grohne <helmut@subdivi.de>:
  * Fix FTCBFS: (Closes: #882536)
    + Let dh_auto_build pass cross compilers to make.
    + Fix build/host confusion in d/rules.
    + Make compilers in d/Makefile substitutable.

 -- Dmitry E. Oboukhov <unera@debian.org>  Fri, 29 Mar 2019 19:45:41 +0300

blockout2 (2.4+dfsg1-8) unstable; urgency=medium

  * Team upload.
  * Drop deprecated menu file.
  * wrap-and-sort -sab.
  * Declare compliance with Debian Policy 4.0.0.
  * Use canonical Vcs-URI.
  * Switch maintainer and uploader. According to the Games Team policy the
    maintainer must be the Debian Games Team. Debian Policy also demands a human
    uploader.
  * Switch to compat level 10 (was 5).
  * Switch from cdbs to dh sequencer.
  * Add keywords and a comment in German.
  * Move Priority: extra to the top of debian/control.
  * Convert icon.xpm to blockout2.png and scale the image to 64x64 pixel.
  * Install blockout2.png into the hicolor icon directory.
  * Update debian/Makefile and make the build reproducible. Thanks to Reiner
    Herrmann for the report and patch. (Closes: #826416)
  * Add format-not-a-string-literal.patch and fix FTBFS due to -format-security
    flag.
  * Override dh_clean and ensure that blockout2 can be built twice in a row.

 -- Markus Koschany <apo@debian.org>  Sat, 15 Jul 2017 17:50:21 +0200

blockout2 (2.4+dfsg1-7) unstable; urgency=low

  * Fix FTBFS with libpng, closes: #662275, #649550.

 -- Dmitry E. Oboukhov <unera@debian.org>  Mon, 02 Jul 2012 22:26:30 +0400

blockout2 (2.4+dfsg1-6) unstable; urgency=low

  * Fix FTBFS with libGL, closes: #618044.

 -- Dmitry E. Oboukhov <unera@debian.org>  Sun, 13 Mar 2011 20:30:34 +0300

blockout2 (2.4+dfsg1-5) unstable; urgency=low

  * Added .desktop file, closes: #608068, thanks for chrysn <chrysn@fsfe.org>.

 -- Dmitry E. Oboukhov <unera@debian.org>  Mon, 27 Dec 2010 09:27:07 +0300

blockout2 (2.4+dfsg1-4) unstable; urgency=low

  * Added patch to build under Debian/kfreebsd.
  * Applied patch from ubuntu: Fix FTBFS caused by "error: invalid
    conversion from 'const char*' to 'char*'".
  * Added DGT to uploaders.

 -- Dmitry E. Oboukhov <unera@debian.org>  Tue, 20 Oct 2009 16:06:29 +0400

blockout2 (2.4+dfsg1-3) unstable; urgency=low

  * Added debian-menu, closes: #525754.

 -- Dmitry E. Oboukhov <unera@debian.org>  Mon, 27 Apr 2009 09:03:02 +0400

blockout2 (2.4+dfsg1-2) unstable; urgency=low

  * Fixed all gcc warnings.

 -- Dmitry E. Oboukhov <unera@debian.org>  Wed, 11 Mar 2009 11:46:16 +0300

blockout2 (2.4+dfsg1-1) unstable; urgency=low

  * Initial release. (Closes: #460537).
  * All libraries with unknown licenses have been dropped, orig.tgz has been
    repackaged. So all the files left have GPL2+ license.
  * Makefile has been rewritten from the start
    (We do not use the original Makefiles).
  * Git-repo has been created in git.debian.org.
  * Sources have been patched for using debian directories structure.

 -- Dmitry E. Oboukhov <unera@debian.org>  Tue, 10 Mar 2009 19:18:26 +0300
